package by.bsuir.oi.sem6;

import by.bsuir.oi.sem6.filters.ConvolutionMatrixFilter;
import by.bsuir.oi.sem6.filters.GrayscaleFilter;
import by.bsuir.oi.sem6.filters.NegativeFilter;
import by.bsuir.oi.sem6.segmentation.Segmentator;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import java.awt.Image;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;

import static by.bsuir.oi.sem6.Utils.restoreImage;

/**
 * Created by user on 17.02.2016.
 */
public class Controller {
    private ImageParameters imageParameters;
    private ConvolutionMatrixFilter gaussianBlur5x5 = new ConvolutionMatrixFilter();

    public Image loadImage() {
        Image image = null;
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setCurrentDirectory(new File(this.getClass().getResource("/").getPath() + "images"));
        int ret = fileChooser.showOpenDialog(null);
        if (ret == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            try {
                image = ImageIO.read(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (image == null) {
            return null;
        }
        imageParameters = new ImageParameters(image);
        return image;
    }

    public Image applyGaussianBlur() throws Exception {
        if (imageParameters == null) {
            throw new Exception("Not image selected");
        }
        return restoreImage(gaussianBlur5x5.applyFilter(imageParameters));
    }

    public Image applyNegative() throws Exception {
        if (imageParameters == null) {
            throw new Exception("Not image selected");
        }
        return restoreImage(NegativeFilter.applyFilter(imageParameters));
    }

    public Image applyGrayscaleFilter() throws Exception {
        if (imageParameters == null) {
            throw new Exception("Not image selected");
        }
        return restoreImage(GrayscaleFilter.applyFilter(imageParameters));
    }

    public Image applyPencilSketchFilter() throws Exception {
        if (imageParameters == null) {
            throw new Exception("Not image selected");
        }
        final ImageParameters imageParams = GrayscaleFilter.applyFilter(imageParameters);
        final ImageParameters maskParams = gaussianBlur5x5.applyFilter(NegativeFilter.applyFilter(imageParams));

        saveOutput(imageParams, "image.png");
        saveOutput(maskParams, "mask.png");

        return restoreImage(BlendModes.dodge(imageParams, maskParams));
    }

    public Image makeAnaglyph() throws Exception {
        if (imageParameters == null) {
            throw new Exception("Not image selected");
        }
        return restoreImage(AnaglyphMaker.makeAnaglyph(imageParameters));
    }

    public Image segment(String string) throws Exception {
        if (imageParameters == null) {
            throw new Exception("Not image selected");
        }
        int numOfClusters;
        Segmentator segmentator;
        try {
            numOfClusters = Integer.parseInt(string);
            if (numOfClusters < 1) {
                numOfClusters = 1;
            }
            segmentator = new Segmentator(numOfClusters);
        } catch (Exception e) {
            segmentator = new Segmentator();
        }
        ImageParameters outImage = segmentator.segment(imageParameters);
        saveOutput(outImage, "output.png");
        return restoreImage(outImage);
    }

    private void saveOutput(ImageParameters outImage, String fileName) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    ImageIO.write((RenderedImage) restoreImage(outImage), "png", new File(fileName));
                } catch (Exception e) {
                    // do nothing
                }
            }
        }).start();
    }
}
