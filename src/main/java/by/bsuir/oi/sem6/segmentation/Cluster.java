package by.bsuir.oi.sem6.segmentation;

/**
 * Created by Dzmitry Saladukha on 29.03.2016.
 */
public class Cluster {
    private int blue;
    private int green;
    private int red;
    private int blueSum;
    private int greenSum;
    private int redSum;
    private int pixelCount;

    public Cluster(int blue, int green, int red) {
        this.blueSum = blue;
        this.blue = blue;
        this.greenSum = green;
        this.green = green;
        this.redSum = red;
        this.red = red;
        this.pixelCount = 1;
    }

    public int getBlue() {
        return blue;
    }

    public int getGreen() {
        return green;
    }

    public int getRed() {
        return red;
    }

    public int distanceToPixel(int blue, int green, int red) {
        int dBlue = this.blue - blue;
        int dGreen = this.green - green;
        int dRed = this.red - red;
        int distance = (int) Math.sqrt(dBlue * dBlue + dGreen * dGreen + dRed * dRed);
        return distance;
    }

    public void clear() {
        blueSum = 0;
        greenSum = 0;
        redSum = 0;
        pixelCount = 0;
    }

    public void addPixel(int blue, int green, int red) {
        blueSum += blue;
        greenSum += green;
        redSum += red;
        pixelCount++;
        this.blue = blueSum / pixelCount;
        this.green =  greenSum / pixelCount;
        this.red = redSum / pixelCount;
    }
}
