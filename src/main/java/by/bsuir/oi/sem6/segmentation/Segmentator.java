package by.bsuir.oi.sem6.segmentation;

import by.bsuir.oi.sem6.ImageParameters;

import java.util.Arrays;

/**
 * Created by Dzmitry Saladukha on 29.03.2016.
 */
public class Segmentator {
    private int numOfClusters = 6;
    private Cluster[] clusters;

    public Segmentator(int numOfClusters) {
        this.numOfClusters = numOfClusters;
        clusters = new Cluster[numOfClusters];
    }

    public Segmentator() {
        clusters = new Cluster[numOfClusters];
    }

    public ImageParameters segment(ImageParameters image) {
        int[] matchingPixels = new int[image.width * image.height];
        Arrays.fill(matchingPixels, -1);
        initClusters(image);
        boolean flag = true;
        while (flag) {
            flag = false;
            for (int index = 0, clusterIndex; index < matchingPixels.length; index++) {
                clusterIndex = findNearestCluster(image.pixels[index * 3], image.pixels[index * 3 + 1], image.pixels[index * 3 + 2]);
                if (matchingPixels[index] != clusterIndex) {
                    flag = true;
                    matchingPixels[index] = clusterIndex;
                }
            }
            for (Cluster cluster : clusters)
                cluster.clear();
            for (int index = 0; index < matchingPixels.length; index++) {
                clusters[matchingPixels[index]].addPixel(image.pixels[index * 3], image.pixels[index * 3 + 1], image.pixels[index * 3 + 2]);
            }
        }
        return new ImageParameters(toPixels(matchingPixels, image), image);
    }

    private int[] toPixels(int[] pixels, ImageParameters image) {
        int[] pixArr = new int[image.pixels.length];
        for (int index = 0, clusterIndex; index < pixels.length; index++) {
            clusterIndex = pixels[index];
            pixArr[index * 3] = clusters[clusterIndex].getBlue();
            pixArr[index * 3 + 1] = clusters[clusterIndex].getGreen();
            pixArr[index * 3 + 2] = clusters[clusterIndex].getRed();
        }
        return pixArr;
    }

    private int findNearestCluster(int blue, int green, int red) {
        int min = 0x1ff;
        int nearestCluster = 0;
        for (int index = 0, distance; index < clusters.length; index++) {
            distance = clusters[index].distanceToPixel(blue, green, red);
            if (distance < min) {
                min = distance;
                nearestCluster = index;
            }
        }
        return nearestCluster;
    }

    private void initClusters(ImageParameters image) {
        int x = 0;
        int y = 0;
        int dx = image.width / numOfClusters;
        int dy = image.height / numOfClusters;
        for (int index = 0, pixel; index < numOfClusters; x += dx, y += dy) {
            pixel = x + y * image.width;
            clusters[index++] = new Cluster(image.pixels[pixel++], image.pixels[pixel++], image.pixels[pixel]);
        }
    }
}
