package by.bsuir.oi.sem6.transform;

import by.bsuir.oi.sem6.ImageParameters;

/**
 * Created by Dzmitry Saladukha on 26.04.2016.
 */
public class Rotator {
    private int newWidth;
    private int newHeight;
    private int width;
    private int height;
    private double cos;
    private double sin;
    private int oldCenterX;
    private int oldCenterY;
    private int centerX;
    private int centerY;

    public ImageParameters rotate(ImageParameters image, double angle) {
        oldCenterX = image.width / 2;
        oldCenterY = image.height / 2;
        calcTrigonomFuncs(angle);
        calcNewSizes(image, angle);
        centerX = newWidth / 2;
        centerY = newHeight / 2;
        this.width = image.width;this.height = image.height;
        final int length = newWidth * newHeight * 3;
        int[] pixels = new int[length];
        for (int index = 0, pixelIndex; index < length; index++) {
            pixelIndex = calcPixelIndex(index / 3) * 3;
            if (pixelIndex > image.pixels.length - 3 || pixelIndex < 0) {
                pixels[index] = 85;
                pixels[++index] = 218;
                pixels[++index] = 186;
            } else {
                pixels[index] = image.pixels[pixelIndex];
                pixels[++index] = image.pixels[++pixelIndex];
                pixels[++index] = image.pixels[++pixelIndex];
            }
        }
        return new ImageParameters(newWidth, newHeight, false, pixels);
    }

    private void calcNewSizes(ImageParameters image, double angle) {
        double angleRad = Math.toRadians(angle);
        double means = Math.sqrt(image.width * image.width + image.height * image.height);
        double atan = Math.atan((double) image.height / (double) image.width);
        double atanN = Math.atan((double) image.width / (double) image.height);
        newWidth = (int) Math.abs((means * Math.sin(atanN + angleRad)));
        newHeight = (int) Math.abs((means * Math.sin(atan + angleRad)));
    }

    private void calcTrigonomFuncs(double angle) {
        double angleRad = Math.toRadians(angle);
        cos = Math.cos(angleRad);
        sin = Math.sin(angleRad);
    }

    private int calcPixelIndex(int index) {
        int x = index % newWidth - centerX;
        int y = index / newWidth - centerY;
        int oldX = (int) Math.round(cos * x + sin * y) + oldCenterX;if(oldX < 0 || oldX > width) return Integer.MAX_VALUE;
        int oldY = (int) Math.round(cos * y - sin * x) + oldCenterY;if(oldY < 0 || oldY > height) return Integer.MAX_VALUE;
        int result = oldX + oldY * width;
        return result;
    }
}
