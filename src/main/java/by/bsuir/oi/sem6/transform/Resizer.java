package by.bsuir.oi.sem6.transform;

import by.bsuir.oi.sem6.ImageParameters;

/**
 * Created by Dzmitry Saladukha on 26.04.2016.
 */
public class Resizer {
    private int newWidth;
    private int newHeight;
    private int width;
    private double size;

    public ImageParameters resize(ImageParameters image, double size) {
        newWidth = (int) (image.width * size);
        newHeight = (int) (image.height * size);
        this.size = size;
        this.width = image.width;
        int[] pixels = new int[newWidth * newHeight * 3];
        final int length = pixels.length;
        for (int index = 0, pixelIndex; index < length; index++) {
            pixelIndex = calcPixelIndex(index / 3) * 3;
            pixels[index] = image.pixels[pixelIndex];
            pixels[++index] = image.pixels[++pixelIndex];
            pixels[++index] = image.pixels[++pixelIndex];
        }
        return new ImageParameters(newWidth, newHeight, false, pixels);
    }

    private int calcPixelIndex(int index) {
        return (int) ((index % newWidth) / size) + (int) (index / newWidth / size) * width;
    }
}
