package by.bsuir.oi.sem6;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.PixelGrabber;

/**
 * Created by Dzmitry Saladukha on 15.3.16.
 */
public class ImageParameters {
    final public int width;
    final public int height;
    final public boolean hasAlphaChannel;
    final public int[] pixels;

    public ImageParameters(int width, int height, boolean hasAlphaChannel, int[] pixels) {
        this.width = width;
        this.height = height;
        this.hasAlphaChannel = hasAlphaChannel;
        this.pixels = pixels;
    }

    public ImageParameters(int[] pixels, ImageParameters imageParameters) {
        this.pixels = pixels;
        this.width = imageParameters.width;
        this.height = imageParameters.height;
        this.hasAlphaChannel = imageParameters.hasAlphaChannel;
    }

    public ImageParameters(Image image) {
        hasAlphaChannel = ((BufferedImage) image).getAlphaRaster() != null;
        width = image.getWidth(null);
        height = image.getHeight(null);

        int[] colorsPG = new int[width * height];
        PixelGrabber pg = new PixelGrabber(image, 0, 0, width, height, colorsPG,
                0, width);
        try {
            pg.grabPixels();
        } catch (InterruptedException e) {
            System.err.println("Ошибка преобразования");
        }
        if (hasAlphaChannel) {
            pixels = new int[width * height * 4];
            for (int x = 0, index = 0; x < colorsPG.length; x++) {
                pixels[index++] = (colorsPG[x] & 0xff000000) >> 24;
                pixels[index++] = (colorsPG[x] & 0xff0000) >> 16;
                pixels[index++] = (colorsPG[x] & 0xff00) >> 8;
                pixels[index++] = colorsPG[x] & 0xff;
            }
        } else {
            pixels = new int[width * height * 3];
            for (int x = 0, index = 0; x < colorsPG.length; x++) {
                pixels[index++] = (colorsPG[x] & 0xff0000) >> 16;
                pixels[index++] = (colorsPG[x] & 0xff00) >> 8;
                pixels[index++] = colorsPG[x] & 0xff;
            }
        }
    }
}
