package by.bsuir.oi.sem6;

/**
 * Created by Dzmitry Saladukha on 26.03.2016.
 */
public class AnaglyphMaker {
    public static ImageParameters makeAnaglyph(ImageParameters stereoImage) {
        final int length = stereoImage.pixels.length;
        int[] pixels = new int[length / 2];
        if (stereoImage.hasAlphaChannel) {
            int index = 0;
            for (int h = 0; h < stereoImage.height; h++)
                for (int w = 0; w < stereoImage.width / 2 * 4; ) {
                    pixels[index++] = stereoImage.pixels[h * stereoImage.width * 4 + w++];
                    pixels[index++] = stereoImage.pixels[h * stereoImage.width * 4 + w++ + stereoImage.width / 2 * 4];
                    pixels[index++] = stereoImage.pixels[h * stereoImage.width * 4 + w++];
                    pixels[index++] = stereoImage.pixels[h * stereoImage.width * 4 + w++];
                }
        } else {
            int index = 0;
            for (int h = 0; h < stereoImage.height; h++)
                for (int w = 0; w < stereoImage.width / 2 * 3; ) {
                    pixels[index++] = stereoImage.pixels[h * stereoImage.width * 3 + w++ + stereoImage.width / 2 * 3];
                    pixels[index++] = stereoImage.pixels[h * stereoImage.width * 3 + w++];
                    pixels[index++] = stereoImage.pixels[h * stereoImage.width * 3 + w++];
                }
        }
        return new ImageParameters(stereoImage.width / 2, stereoImage.height, stereoImage.hasAlphaChannel, pixels);
    }
}
