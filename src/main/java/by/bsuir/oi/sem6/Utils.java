package by.bsuir.oi.sem6;

import org.opencv.core.Mat;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;

/**
 * Created by Dzmitry Saladukha on 13.05.2016.
 */
public class Utils {
    public static Image restoreImage(ImageParameters imageParameters) {
        if (imageParameters == null) {
            return null;
        }
        final int length = imageParameters.pixels.length;
        int[] pixels = new int[imageParameters.width * imageParameters.height];
        if (imageParameters.hasAlphaChannel) {
            for (int index = 0, pixel = 0, abgr; index < length; ) {
                abgr = imageParameters.pixels[index++] << 24;
                abgr |= imageParameters.pixels[index++] << 16;
                abgr |= imageParameters.pixels[index++] << 8;
                abgr |= imageParameters.pixels[index++];

                pixels[pixel++] = abgr;
            }
        } else {
            for (int index = 0, pixel = 0, bgr; index < length; ) {
                bgr = imageParameters.pixels[index++] << 16;
                bgr |= imageParameters.pixels[index++] << 8;
                bgr |= imageParameters.pixels[index++];

                pixels[pixel++] = bgr;
            }
        }

        BufferedImage outputImage =
                new BufferedImage(imageParameters.width, imageParameters.height, BufferedImage.TYPE_3BYTE_BGR);
        outputImage.setRGB(0, 0, imageParameters.width, imageParameters.height, pixels, 0, imageParameters.width);
        return outputImage;
    }

    public BufferedImage toBufferedImage(Mat matrix) {
        int type = BufferedImage.TYPE_BYTE_GRAY;
        if (matrix.channels() > 1) {
            type = BufferedImage.TYPE_3BYTE_BGR;
        }
        int bufferSize = matrix.channels() * matrix.cols() * matrix.rows();
        byte[] buffer = new byte[bufferSize];
        matrix.get(0, 0, buffer); // get all the pixels
        BufferedImage image = new BufferedImage(matrix.cols(), matrix.rows(), type);
        final byte[] targetPixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
        System.arraycopy(buffer, 0, targetPixels, 0, buffer.length);
        return image;
    }
}
