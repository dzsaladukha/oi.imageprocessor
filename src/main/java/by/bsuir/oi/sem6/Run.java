package by.bsuir.oi.sem6;

import by.bsuir.oi.sem6.view.MainFrame;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 * Created by user on 17.02.2016.
 */
public class Run {

    public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        // TODO Auto-generated method stub
        try {
            UIManager.setLookAndFeel(
                    "javax.swing.plaf.nimbus.NimbusLookAndFeel");
        } catch (UnsupportedLookAndFeelException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        javax.swing.SwingUtilities.invokeLater(()-> new MainFrame());
    }

}
