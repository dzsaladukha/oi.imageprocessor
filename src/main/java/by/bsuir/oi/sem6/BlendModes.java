package by.bsuir.oi.sem6;

/**
 * Created by Dzmitry Saladukha on 29.03.2016.
 */
public class BlendModes {
    public static ImageParameters dodge(ImageParameters image, ImageParameters mask) {
        final int length = image.pixels.length;
        int[] pixels = new int[length];
        int value;
        for (int index = 0; index < length; index++) {
            value = image.pixels[index] * 256 / (256 - mask.pixels[index]);
            if (value > 255)
                value = 255;
            pixels[index] = value;
        }

        return new ImageParameters(pixels, image);
    }
}
