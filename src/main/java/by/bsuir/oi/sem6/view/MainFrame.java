package by.bsuir.oi.sem6.view;

import by.bsuir.oi.sem6.Controller;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.WindowConstants;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by user on 17.02.2016.
 */
public class MainFrame extends JFrame {
    private ImagePanel inImagePanel = new ImagePanel();
    private ImagePanel outImagePanel = new ImagePanel();

    public MainFrame() {
        super("ОИ, лабораторные работы №1-3");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(new Dimension(
                Toolkit.getDefaultToolkit().getScreenSize().width / 5 * 4,
                Toolkit.getDefaultToolkit().getScreenSize().height / 5 * 4));
        setLocationRelativeTo(null);
        setLayout(new BorderLayout());

        JPanel imageContPane = new JPanel();
        imageContPane.setLayout(new BoxLayout(imageContPane, BoxLayout.X_AXIS));
        inImagePanel.setBorder(BorderFactory.createEtchedBorder());
        imageContPane.add(inImagePanel);
        outImagePanel.setBorder(BorderFactory.createEtchedBorder());
        imageContPane.add(outImagePanel);
        add(imageContPane, BorderLayout.CENTER);

        add(createToolbar(), BorderLayout.NORTH);

        setVisible(true);
    }

    private Component createToolbar() {
        JToolBar toolBar = new JToolBar("Toolbar");
        JButton loadButton = new JButton("Load image");
        JButton anaglyphButton = new JButton("Make anaglyph");
        JButton segmentButton = new JButton("Segment");
        JTextField textField = new JTextField(10);
        final JComboBox<String> comboBox = new JComboBox<>(
                new String[]{"---", "Gaussian Blur", "Negative", "Grayscale", "Pencil sketch"});
        final Controller controller = new Controller();

        loadButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                inImagePanel.setImage(controller.loadImage());
            }
        });

        anaglyphButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    outImagePanel.setImage(controller.makeAnaglyph());
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        });
        anaglyphButton.setToolTipText("Horizontally oriented pair of images must be selected");

        comboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        switch ((String) comboBox.getSelectedItem()) {
                            case "Gaussian Blur":
                                try {
                                    outImagePanel.setImage(controller.applyGaussianBlur());
                                } catch (Exception e1) {
                                    e1.printStackTrace();
                                }
                                break;
                            case "Negative":
                                try {
                                    outImagePanel.setImage(controller.applyNegative());
                                } catch (Exception e1) {
                                    e1.printStackTrace();
                                }
                                break;
                            case "Grayscale":
                                try {
                                    outImagePanel.setImage(controller.applyGrayscaleFilter());
                                } catch (Exception e1) {
                                    e1.printStackTrace();
                                }
                                break;
                            case "Pencil sketch":
                                try {
                                    outImagePanel.setImage(controller.applyPencilSketchFilter());
                                } catch (Exception e1) {
                                    e1.printStackTrace();
                                }
                                break;
                        }
                    }
                }).start();
            }
        });

        segmentButton.addActionListener((e) -> {
            new Thread(() -> {
                try {
                    outImagePanel.setImage(controller.segment(textField.getText()));
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }).start();
        });

        toolBar.add(loadButton);
        toolBar.add(anaglyphButton);
        toolBar.add(comboBox);
        toolBar.add(new JLabel("Number of clusters:"));
        toolBar.add(textField);
        toolBar.add(segmentButton);
        return toolBar;
    }
}
