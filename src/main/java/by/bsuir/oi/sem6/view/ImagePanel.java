package by.bsuir.oi.sem6.view;

import by.bsuir.oi.sem6.Detector;
import by.bsuir.oi.sem6.ImageParameters;
import by.bsuir.oi.sem6.Utils;
import by.bsuir.oi.sem6.transform.Resizer;
import by.bsuir.oi.sem6.transform.Rotator;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;
import javax.swing.border.BevelBorder;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;

/**
 * Created by Dzmitry Saladukha on 17.02.2016.
 */
public class ImagePanel extends JScrollPane {

    private JLabel imageLabel;
    private ImageParameters imageParameters;
    private MouseAdapter mouseAdapter;
    private Image image;

    public ImagePanel() {
        imageLabel = new JLabel();
        this.setViewportView(imageLabel);

        JPopupMenu popupMenu = createPopupMenu();

        mouseAdapter = new MouseAdapter() {

            private int mouseStartX, mouseStartY;

            @Override
            public void mouseDragged(MouseEvent e) {
                if (SwingUtilities.isLeftMouseButton(e)) {
                    JViewport viewPort = getViewport();
                    Point vpp = viewPort.getViewPosition();
                    vpp.translate((int) ((mouseStartX - e.getX()) * 0.3), (int) ((mouseStartY - e.getY()) * 0.3));
                    imageLabel.scrollRectToVisible(new Rectangle(vpp, viewPort.getSize()));
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {
                mouseStartX = e.getX();
                mouseStartY = e.getY();
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if (e.isPopupTrigger() && SwingUtilities.isRightMouseButton(e)) {
                    popupMenu.show(e.getComponent(), e.getX(), e.getY());
                }
            }
        };
    }

    public void setImage(Image image) {
        if (image == null) {
            return;
        }
        this.getViewport().addMouseListener(mouseAdapter);
        this.getViewport().addMouseMotionListener(mouseAdapter);
        new Thread(() -> {
            imageParameters = new ImageParameters(image);
        }).start();
        this.image = image;
        imageLabel.setIcon(new ImageIcon(image));
    }

    private void updateImage(Image image) {
        if (image == null) {
            return;
        }
        imageLabel.setIcon(new ImageIcon(image));
    }

    private JPopupMenu createPopupMenu() {
        JPopupMenu popupMenu = new JPopupMenu();

        JMenu menu;
        popupMenu.add(menu = new JMenu("Rotate to ..."));
        JMenuItem item;
        Rotator rotator = new Rotator();
        menu.add(item = new JMenuItem("0\u00b0"));
        item.addActionListener(event -> updateImage(Utils.restoreImage(imageParameters)));
        menu.add(item = new JMenuItem("90\u00b0"));
        item.addActionListener(event -> updateImage(Utils.restoreImage(rotator.rotate(imageParameters, 90))));
        menu.add(item = new JMenuItem("-90\u00b0"));
        item.addActionListener(event -> updateImage(Utils.restoreImage(rotator.rotate(imageParameters, 270))));
        menu.add(item = new JMenuItem("180\u00b0"));
        item.addActionListener(event -> updateImage(Utils.restoreImage(rotator.rotate(imageParameters, 180))));
        menu.add(item = new JMenuItem("free angle"));
        item.addActionListener(event -> {
            double angle;
            try {
                angle = Double.parseDouble(JOptionPane.showInputDialog(this, "Type angle"));
                if (angle < 0) {
                    angle = -angle;
                }
                if (angle > 360) {
                    angle = angle % 360;
                }
            } catch (Exception e) {
                return;
            }
            updateImage(Utils.restoreImage(rotator.rotate(imageParameters, angle)));
        });
        popupMenu.addSeparator();

        popupMenu.add(menu = new JMenu("Resize to ..."));
        Resizer resizer = new Resizer();
        menu.add(item = new JMenuItem("50%"));
        item.addActionListener(event -> updateImage(Utils.restoreImage(resizer.resize(imageParameters, 0.5))));
        menu.add(item = new JMenuItem("100%"));
        item.addActionListener(event -> updateImage(Utils.restoreImage(resizer.resize(imageParameters, 1))));
        menu.add(item = new JMenuItem("200%"));
        item.addActionListener(event -> updateImage(Utils.restoreImage(resizer.resize(imageParameters, 2))));
        menu.add(item = new JMenuItem("300%"));
        item.addActionListener(event -> updateImage(Utils.restoreImage(resizer.resize(imageParameters, 3))));
        menu.add(item = new JMenuItem("free size"));
        item.addActionListener(event -> {
            double xsize;
            try {
                xsize = Double.parseDouble(JOptionPane.showInputDialog(this, "Type xsize"));
            } catch (Exception e) {
                return;
            }
            updateImage(Utils.restoreImage(resizer.resize(imageParameters, xsize)));
        });
        popupMenu.addSeparator();

        JMenuItem item1;
        popupMenu.add(item1 = new JMenuItem("Detect phone"));
        Detector detector = new Detector();
        item1.addActionListener(event -> {
            updateImage(detector.findObject((BufferedImage)this.image));
        });

        popupMenu.setBorder(new BevelBorder(BevelBorder.RAISED));
        return popupMenu;
    }
}