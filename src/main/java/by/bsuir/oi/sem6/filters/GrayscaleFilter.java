package by.bsuir.oi.sem6.filters;

import by.bsuir.oi.sem6.ImageParameters;

/**
 * Created by Dzmitry Saladukha on 15.3.16.
 */
public class GrayscaleFilter {
    public static ImageParameters applyFilter(ImageParameters image) {
        final int length = image.pixels.length;
        int[] pixels = new int[length];
        if (image.hasAlphaChannel) {
            for (int pixel = 0, index = 0, grayLevel; pixel < length; ) {
                pixels[index++] = image.pixels[pixel++];

                grayLevel = (int) (image.pixels[pixel++] * 0.0722);
                grayLevel += (int) (image.pixels[pixel++] * 0.7152);
                grayLevel += (int) (image.pixels[pixel++] * 0.2126);

                pixels[index++] = grayLevel;
                pixels[index++] = grayLevel;
                pixels[index++] = grayLevel;
            }
        } else {
            for (int pixel = 0, index = 0, grayLevel; pixel < length; ) {
                grayLevel = (int) (image.pixels[pixel++] * 0.0722);
                grayLevel += (int) (image.pixels[pixel++] * 0.7152);
                grayLevel += (int) (image.pixels[pixel++] * 0.2126);

                pixels[index++] = grayLevel;
                pixels[index++] = grayLevel;
                pixels[index++] = grayLevel;
            }
        }

        return new ImageParameters(pixels, image);
    }
}
