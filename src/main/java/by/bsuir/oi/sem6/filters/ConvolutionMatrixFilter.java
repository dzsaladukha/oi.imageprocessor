package by.bsuir.oi.sem6.filters;

import by.bsuir.oi.sem6.ImageParameters;

/**
 * Created by Dzmitry Saladukha on 15.3.16.
 */
public class ConvolutionMatrixFilter {
    private double[][] kernel =
            {{0.000789, 0.006581, 0.013347, 0.006581, 0.000789}, {0.006581, 0.054901, 0.111345, 0.054901, 0.006581},
                    {0.013347, 0.111345, 0.225821, 0.111345, 0.013347},
                    {0.006581, 0.054901, 0.111345, 0.054901, 0.006581},
                    {0.000789, 0.006581, 0.013347, 0.006581, 0.000789}};
    private int kernelSize = 5;
    private int stDev = 1;
    private ImageParameters image;

    public ConvolutionMatrixFilter(int stDev, double[][] kernel) {
        this.stDev = stDev;
        this.kernel = kernel;
        this.kernelSize = kernel.length;
    }

    public ConvolutionMatrixFilter(double[][] kernel) {
        this.kernel = kernel;
        this.kernelSize = kernel.length;
    }

    public ConvolutionMatrixFilter(int stDev) {
        this.stDev = stDev;
    }

    public ConvolutionMatrixFilter() {
    }

    public void setStDev(int stDev) {
        this.stDev = stDev;
    }

    public ImageParameters applyFilter(ImageParameters image) {
        this.image = image;
        int[] pixels = new int[image.width * image.height * 3];
        int index;
        int[] pixel;
        int blindZone = kernelSize / 2;
        for (int i = blindZone; i < this.image.width - blindZone; i++) {
            for (int j = blindZone; j < this.image.height - blindZone; j++) {
                index = arrayIndex(i, j) * 3;
                pixel = multiply(getMatrixByCenter(i, j));
                pixels[index++] = pixel[0];
                pixels[index++] = pixel[1];
                pixels[index] = pixel[2];
            }
        }

        return new ImageParameters(pixels, this.image);
    }

    private int[] multiply(double[][][] imageMatrix) {
        int blue = 0;
        for (int col = 0; col < kernelSize; col++) {
            for (int row = 0; row < kernelSize; row++) {
                blue += imageMatrix[col][row][0] * kernel[col][row];
            }
        }
        blue /= stDev;
        int green = 0;
        for (int col = 0; col < kernelSize; col++) {
            for (int row = 0; row < kernelSize; row++) {
                green += imageMatrix[col][row][1] * kernel[col][row];
            }
        }
        green /= stDev;
        int red = 0;
        for (int col = 0; col < kernelSize; col++) {
            for (int row = 0; row < kernelSize; row++) {
                red += imageMatrix[col][row][2] * kernel[col][row];
            }
        }
        red /= stDev;
        return new int[]{blue, green, red};
    }

    private double[][][] getMatrixByCenter(int i, int j) {
        double[][][] imageMatrix = new double[kernelSize][kernelSize][3];
        final int kernelHalf = kernelSize / 2;
        final int kernelHalfAnd1 = kernelHalf + 1;
        for (int x = -kernelHalf; x < kernelHalfAnd1; x++) {
            for (int y = -kernelHalf, index = arrayIndex(i + x, j + y) * 3;
                 y < kernelHalfAnd1;
                 y++, index = arrayIndex(i + x, j + y) * 3) {
                int n = x + kernelHalf;
                int m = y + kernelHalf;
                imageMatrix[n][m][0] = image.pixels[index];
                imageMatrix[n][m][1] = image.pixels[index + 1];
                imageMatrix[n][m][2] = image.pixels[index + 2];
            }
        }
        return imageMatrix;
    }

    private int arrayIndex(int x, int y) {
        return (x + y * image.width);
    }


}
