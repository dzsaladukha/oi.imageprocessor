package by.bsuir.oi.sem6.filters;

import by.bsuir.oi.sem6.ImageParameters;

/**
 * Created by Dzmitry Saladukha on 15.3.16.
 */
public class NegativeFilter {
    public static ImageParameters applyFilter(ImageParameters image) {
        final int length = image.pixels.length;
        int[] pixels = new int[length];
        if (image.hasAlphaChannel) {
            for (int pixel = 0; pixel < length; ) {
                pixels[pixel] = (image.pixels[pixel++]);
                pixels[pixel] = (0xff - image.pixels[pixel++]);
                pixels[pixel] = (0xff - image.pixels[pixel++]);
                pixels[pixel] = (0xff - image.pixels[pixel++]);
            }
        } else {
            for (int pixel = 0; pixel < length; ) {
                pixels[pixel] = 0xff - image.pixels[pixel++];
            }
        }

        return new ImageParameters(pixels, image);
    }
}
