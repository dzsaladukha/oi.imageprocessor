package by.bsuir.oi.sem6;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;

/**
 * Created by Dzmitry Saladukha on 13.05.2016.
 */
public class Detector {
    static {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }

    private CascadeClassifier detector;

    public Detector() {
        loadCascade();
    }

    private void loadCascade() {
        String cascadePath = (this.getClass().getResource("/").getPath() + "cascade.xml").substring(1);
        detector = new CascadeClassifier(cascadePath);
    }

    public Image findObject(BufferedImage image) {
        Image tempImage;
        Mat mat = new Mat(image.getHeight(), image.getWidth(), CvType.CV_8UC3);
        mat.put(0, 0, ((DataBufferByte) image.getRaster().getDataBuffer()).getData());
        Utils utils = new Utils();
        detectAndDraw(mat);
        tempImage = utils.toBufferedImage(mat);
        return tempImage;
    }

    private void detectAndDraw(Mat image) {
        MatOfRect matOfRect = new MatOfRect();
        detector.detectMultiScale(image, matOfRect, 1.1, 7, 0, new Size(250, 40), new Size());
        for (Rect rect : matOfRect.toArray()) {
            Imgproc.rectangle(image, new Point(rect.x, rect.y), new Point(rect.x + rect.width, rect.y + rect.height),
                    new Scalar(85, 218, 186));
        }
    }
}
